+++
title = "#photochallenge day 1: Just Paris"
date = 2018-09-02T19:03:18+02:00
description = "I decided to start a photchallenge, and a photoblog!"
draft = false
tags = ["Paris"]
categories = ["photostories"]
+++



![justparis](images/paris_essence.jpg)

Since I find myself walking a lot going from one place to another, I decided to use this time training my eyes and my camera skills. I'll try to post one photo each day for one week, just to see what happens.

Yesterday was my first day and I have to say it was harder than expected. In the beginning, I was just randomly walking around looking for "something interesting", without a specific topic. It was just not working. Paris is a huge city full of interesting things around, so aiming for just one randomly was just a wasting of time. I decided to give me a goal: describe Paris with just one photo. I started looking for people wearing a "Beret", the traditional Parisian hat, or people carrying "Baguette", the traditional Parisian bread, but seems like nobody is doing traditional things anymore! So I started asking myself: what Parisian do nowadays? Sport, hanging out in a café, or just hate tourists who don't speak French (yes, they really do). But all these answers did not satisfy me, you can find this kind of things in all the other cities in the world (besides the language thing).

What makes Paris different from everything else? Why do people fall in love with this city? 

The answer showed up during the sunset. I was smoking a cigarette while looking at the photos I shot, trying to choose one that I liked when this group o people showed up. They were four girls and a guy walking around, dressed like superheroes, stopping people to talk about the LGBT Rights. But this was not what amazed me. Seems like I was the only one who noticed them. People all around were just not caring about the way they were making noises, about their capes and water guns, it was just a normal group of people walking around Paris. The blue girl on the left kept taking pictures of a random building when they walked around her. 

That's the essence of this city then. Paris is a multicultural, eccentric, innovative, artistic city, where you are free to walk around doing what you like without attracting attention or being judged as "weird", because you are just another crazy inhabitant of this crazy city.