+++
title = "#photochallenge day 5: Ghost in Paris"
date = 2018-09-07T19:03:18+02:00
description = "Fifth day of the photochallenge, I saw a ghost!!"
draft = false
tags = ["Paris"]
categories = ["photostories"]
+++
![fantasma](images/fantasma.jpg)

Walking around Paris, you can notice an enormous amount of clochards. People walk through them pretending not to notice.

This guy in the photo was running when suddenly his blanket (probably the only thing that separates him from the ground during the night) flown away.

He tried to catch it in mid-air, but the wind didn't help him.

Luckily I started shooting just in time to catch this shot of one of the ghosts of Paris looking like a ghost 