+++
title = "#photochallenge day 4: A regular Red Sign"
date = 2018-09-05T19:03:18+02:00
description = "Fourth day of the photochallenge, reality sucks"
draft = false
tags = ["Paris"]
categories = ["photostories"]
+++



![bataclan](images/bataclan.jpg)
Today, walking to the metro stop Saint-Ambroise, I saw a red sign that I never noticed before. It was pretty weird reading it, considering how famous this place is. But it is a small sign, smaller than you expect, and the location is not fancy at all. I would call I a regular place, where people like me would go to assist an event, or to drink a coffee.

The sign says "Bataclan"

In the news, this building looked way bigger and catchy, but it's the opposite in real life. It is smaller than the surrounding constructions, and it's way less colorful than expected, especially on rainy days.
Almost three years ago 90 people died. 
90 guys, more or less my age, died just because they choose to attend a concert in a small building in Paris. 

Almost three years ago three terrorists decided to enter a small building in Paris, point their guns at some guys having fun at a concert, and kill 90 of them, just to prove something probably not related to any of them.

I remember that I felt sad watching the news some years ago, we all did, but believe me, being here and realizing how regular is this place put you in another dimension. Being here makes you realize that you could have been there as well, in a regular concert hall, dying because of some stupid political reasons after you left home telling your parents you were going to a concert. 
Maybe we should learn to solve problems differently before making them escalade involving harmless people who are just having fun.
