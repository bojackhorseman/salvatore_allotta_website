+++
title = "#photochallenge day 2: Skateboarding"
date = 2018-09-03T19:03:18+02:00
description = "The second day of the photochallenge, I meet a skater!"
draft = false
tags = ["Paris"]
categories = ["photostories"]
+++



![skater](images/skater.jpg)

In every big city, it's pretty easy to find people skateboarding. They just need some space, like a big square, a public park, parking or a specially made skatepark with some ramps, and of course, a skateboard.
Paris is not an exception, and you can find plenty of skateboarders in Place de la Republique, performing tricks on some ramps, benches or wooden boxes they bring, probably, from home.

Stopping and analysing them made me notice a strange thing: they fall down, hit the ground really hard, get back in their feet, and fall down again. For hours. 
Why should somebody perform an action, hunting himself over and over? What is the goal?
I decided to ask them.

The guy in the photo is Paul, 1 second before his ass hit the ground.

Paul comes here every Sunday and Wednesday, from 17.00 to 20.00, to hurt himself, hard. Once he broke a wrist, once his nose, two times a finger, and I stopped counting the injury list here. My first question was "why?" 
He laughed and answered that he just like it. "Hurting yourself? Are you a masochist?". No, he just enjoys reaching his goals. 
He told me that skateboarding is not a team sport, is not even a sport, is a lifestyle. It's a challenge between you and the trick that you are learning. You start from a simple jump, then you want to try a flip, then you want to jump an obstacle or a grind on a bench. You set up your goal, and you just start training, over and over until you feel comfortable doing that trick. Then you start the process again. It's a loop, and they try to stay inside it as long as possible. 
Paul admitted me that sometimes he wants to learn a new trick just to impress girls, other friend or newbies. But he has to be careful not setting up a goal too far from away from his skills. Otherwise, you'll just end up hurting yourself without seeing any improvement. 

We had a 20 min conversation before he went back to his business. I spent almost one hour watching him hoping to see a spectacular grind over that bench. 
He went close, I mean really close, but he didn't succeed. 
In the meantime, I started recalling Paul's words. I think that skateboarders can teach us some lessons: to set up goals, in a smart way; to not be scared of failure and pain, because we will heal; to not have fun of others who are trying to reach their goals, because we all started from the beginning too; and finally, to get back on our feet and try again, over and over, because it's the only way to learn a new trick.

I choose this photo of Paul (among hundreds) because he is falling, this is true, but he is still on his feet, bravely preparing to hit the ground hard, before rising up to try again.Photo challengePhotochallenge #day2 : Skateboarding
In every big city, it's pretty easy to find people skateboarding. They just need some space, like a big square, a public park, parking or a specially made skatepark with some ramps, and of course, a skateboard.
Paris is not an exception, and you can find plenty of skateboarders in Place de la Republique, performing tricks on some ramps, benches or wooden boxes they bring, probably, from home.
Stopping and analysing them made me notice a strange thing: they fall down, hit the ground really hard, get back in their feet, and fall down again. For hours. 

Why should somebody perform an action, hunting himself over and over? What is the goal?
I decided to ask them
The guy in the photo is Paul, 1 second before his ass hit the ground. 
Paul comes here every Sunday and Wednesday, from 17.00 to 20.00, to hurt himself, hard. Once he broke a wrist, once his nose, two times a finger, and I stopped counting the injury list here. My first question was "why?" 
He laughed and answered that he just like it. "Hurting yourself? Are you a masochist?". No, he just enjoys reaching his goals. 
He told me that skateboarding is not a team sport, is not even a sport, is a lifestyle. It's a challenge between you and the trick that you are learning. You start from a simple jump, then you want to try a flip, then you want to jump an obstacle or a grind on a bench. You set up your goal, and you just start training, over and over until you feel comfortable doing that trick. Then you start the process again. It's a loop, and they try to stay inside it as long as possible. 
Paul admitted me that sometimes he wants to learn a new trick just to impress girls, other friend or newbies. But he has to be careful not setting up a goal too far from away from his skills. Otherwise, you'll just end up hurting yourself without seeing any improvement. 

We had a 20 min conversation before he went back to his business. I spent almost one hour watching him hoping to see a spectacular grind over that bench. 
He went close, I mean really close, but he didn't succeed. 

In the meantime, I started recalling Paul's words. I think that skateboarders can teach us some lessons: to set up goals, in a smart way; to not be scared of failure and pain, because we will heal; to not have fun of others who are trying to reach their goals, because we all started from the beginning too; and finally, to get back on our feet and try again, over and over, because it's the only way to learn a new trick.

I choose this photo of Paul (among hundreds) because he is falling, this is true, but he is still on his feet, bravely preparing to hit the ground hard, before rising up to try again.