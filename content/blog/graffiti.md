+++
title = "#photochallenge day 3: Graffiti"
date = 2018-09-04T19:03:18+02:00
description = "Third day of the photochallenge, I found a wall that changes every day!"
draft = false
tags = ["Paris"]
categories = ["photostories"]
+++



![graffiti](images/graffiti.jpg)

Graffiti are writings, drawings, that we can find everywhere in the world. Sometimes they are just simple written words, occasionally contemporary art pieces. It's something that humans do since ever, since we lived in caves, to leave a mark of our passage or to send a message.

Writers, that's the name of graffiti makers, go around the city armed with spray paint cans leaving their marks on walls, metro cars, bridges and sometimes public art pieces.

Most of the time graffiti are considered vandalism, sometimes they are considered street art.

They change names according to the size, going from the smallest one, called "tag", to the more complex and larger, called "roller" or the "blockbuster".

In Paris, there are some specific places where writers can exhibit their skills, or send a message, without getting int troubles. One of this places is "Pointe Poulmarch", a triangle shaped little square with a big wall where an art war is rumbling.
This wall is an open canvas for street artists, and he has a long story of graffiti covering other graffiti, political messages covered by fun messages, self-advertising covered by liters and liters of paint.
Almost every week this wall changes his skin, making it a living art piece to admire day by day.

Sadly the artist of today was not really into conversating, so I don't know the story behind his work.

But if you want to read more about this wall, I found this interesting website:
http://www.lespritbenuchot.fr/canal-saint-martin/la-pointe-poulmarch-quand-lhistoire-rode-sur-le-quai-de-valmy/