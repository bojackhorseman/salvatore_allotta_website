+++
title = "How to use Presets in Lightroom"
date = 2019-01-30T14:57:50+01:00
description = "How to create a custom preset in Lightroom ad import or export new styles"
image = "lightroom_tutorial/Lightroom_tutorial_cover.jpg"
alt ="Lightroom preset tutorial image"
draft = false
tags = ["Tutorials", "lightroom", "photography", "software"]
categories = ["Tutorial", "Lightroom"]
+++

**Lightroom** is one of the most, if not THE most, used software in the photography world. It's a creative tool that gives us the ability to develop our photos according to our **style**.
But sometimes we have a dozen **photos** to edit and we don't have the time to regulate every single parameter. 
Adobe Lightroom has the perfect tool for us, the **Presets**, and in this tutorial, I'll teach you how to get the **best** from them.

**Independently** of the Lightroom version you are using right now, you have the ability to use some **preloaded** Presets, create your own, or **import** styles you downloaded from internet.

### First thing first: How do I access the Presets panel?

 1. Open Lightroom
 2. Select a photo from the Library
 3. Click on Develop tab
 4. The Presets Panel is on your left, If you cannot see it, right click on an empty area of the column and select "Presets".
 ![lightroom presets panel](images/lightroom_tutorial/Presets_panel.jpg "Lightroom Presets Panel")
 5. Select also "Snapshots", you will see why in during the tutorial.

### How to use the Presets
In the preset menù, we can see different folders, clicking on the **triangle** we will access the Presets.

**Hovering** with the mouse on each preset we will see a **preview** of the effect in the navigator window.

**Before** applying the effect click on the "+" icon in the **Snapshots** panel. This will create a **Backup** copy of your setting, so you always come back if you don't like the preset.

To **apply** the style click on the preset's name. You can also click and drag it on the photos in your film strip (activate it pressing F6 or navigating in "*Window - Panels - Filmstrip*")
![lightroom filmstrip panel](images/lightroom_tutorial/Lightroom_film_strip.jpg "lightroom filmstrip panel")

To **apply** your preset to **multiple** images go in the *Library tab*, select the photos (Ctrl + click or using the Painter tool)
![lightroom painter tool](images/lightroom_tutorial/Lightroom_painter_tool.jpg "lightroom painter tool")

**Open** the *Quick Develop* panel (if you don't see it, right click in a blank area) and select your preset in the "*Saved Preset*" menù.

### How to create your preset

Lightroom gives us the ability to **create** a preset for literally everything. From the exposure setting to the curve, to the color setting, camera, and lens calibration, etc, the sky is the limit, but I have to give you a little **advice**.

The Presets are really **powerful**, but they are not smart and **creative** as us, and they **don't** adapt to different situations. 

If you have **different** photos, shot in different **conditions** (multiple expositions, white balance, cameras), the preset **will not** look as you expect, because the starting point is different so it will need some **adjustment**, especially in the exposition panel.
For that reason I **suggest** you to create **multiple** Presets for different developing stages.

**But let's get into action**

 To create a new preset:
 
 1. Go in the Develop tab
 2. **Create** a new snapshot (trust me, this habit will **save** you tons of problems in the future, and it's also free) 
 3. Develop yout photo
 4. Click on the "+" icon in the preset Panel
 5. Now you can select the settings you want to save, you can also click on "check all" to select everything (I do not advise doing that)
 ![lightroom save preset window](images/lightroom_tutorial/Lightroom_save_preset.jpg "lightroom save preset window")
 6. **Rename** your custom preset and select the folder where you want to save it, you can also create a **new folder**

### Bonus tip!

As I wrote, in the beginning, you can export and import the Presets.

To **export** them just right click on the preset's name and select "export"

**Save** the file **.lrtemplate* where you want

To **import** a preset just right click on the Presets list and select "import"

Navigate to the folder where your preset are and select them

You can also **import** a preset folder clicking in "*Edit - Preferences*", then "*Show Lightroom Presets Folder*", **paste** your Presets in a folder and **restart** Lightroom. Now you will have a new folder in the Presets panel.

Hoping that this tutorial was **useful**, consider following me on the socials so you can stay tuned for the next one! 
